#!/bin/bash 
#------------------------------------------------------------------------------------
# EASIngestor.sh
# Script to call the EAS ingestor Python 3 tool
#
# Created by J C Gonzalez <jcgonzalez@sciops.esa.int>
# Copyright (C) 2015-2020 by Euclid SOC Team
#------------------------------------------------------------------------------------
SCRIPTPATH=$(cd $(dirname $(which $0)); pwd; cd - > /dev/null)
export PYTHONPATH=${SCRIPTPATH}
${PYTHON:=python3} ${SCRIPTPATH}/eas_ingestor.py $*

