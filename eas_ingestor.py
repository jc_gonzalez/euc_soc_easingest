#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
LE1 Dissemination Test Execution

Generates a set of
Generates
"""
#----------------------------------------------------------------------

import os
import sys
import argparse
import shutil
import zipfile
from glob import glob

from ingest_client_sc456 import main as ab_ingest_fn

#----------------------------------------------------------------------

_filedir_ = os.path.dirname(os.path.realpath(__file__))
_appsdir_, _ = os.path.split(_filedir_)
_basedir_, _ = os.path.split(_appsdir_)
sys.path.insert(0, os.path.abspath(os.path.join(_filedir_, _basedir_, _appsdir_)))

PYTHON2 = False
PY_NAME = "python3"

import logging
logger = logging.getLogger()

#----------------------------------------------------------------------

VERSION = '0.0.1'

__author__     = "J. C. Gonzalez"
__version__    = VERSION
__license__    = "LGPL 3.0"
__status__     = "Development"
__copyright__  = "Copyright (C) 2015-2020 by Euclid SOC Team @ ESAC / ESA"
__email__      = "jcgonzalez@sciops.esa.int"
__date__       = "2020-05-03"
__maintainer__ = "Euclid SOC Team"
#__url__       = ""

#----------------------------------------------------------------------

def unZipToFolder(zip_file, output_dir):
    """
    Extracts the files from a zip file into the provided folder (it should exist)
    """
    with zipfile.ZipFile(zip_file, 'r') as zipf:
        zipf.extractall(output_dir)

def configureLogs(level):
    logger.setLevel(level)

    # Create handlers
    c_handler = logging.StreamHandler()
    c_handler.setLevel(level)

    # Create formatters and add it to handlers
    c_format = logging.Formatter('%(asctime)s %(levelname).1s %(module)s:%(lineno)d %(message)s')
    c_handler.setFormatter(c_format)

    # Add handlers to the logger
    logger.addHandler(c_handler)
    if 'LOGGING_MODULES' in os.environ:
        for lname in os.environ['LOGGING_MODULES'].split(':'):
            lgr = logging.getLogger(lname)
            if not lgr.handlers: lgr.addHandler(c_handler)

def getArgs():
    """
    Parse arguments from command line
    :return: args structure
    """
    parser = argparse.ArgumentParser(description='Script to ingest data files ' +
                                     '(and metadata information, in XML files)'
                                     ' into the EAS',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-d', '--debug', dest='debug', action='store_true',
                        help='Activate debug information')
    parser.add_argument('-q', '--quiet', dest='quiet', action='store_true',
                        help='Deactivate any informative output (except errors)')
    parser.add_argument(dest='files_or_folders', metavar='ITEM', nargs='+',
                        help='Input files or folder with files to be ingested')

    return parser.parse_args()

def ingest_files(folder):
    """
    Ingest files into EAS
    """
    Credentials = {"u": 'EC_SOC', "p": 'Eu314_clid'}

    saved_argv = sys.argv
    try:
        # Show info
        logger.info(f'Ingesting data in {folder}:')
        gen_files = glob(f'{folder}/*')
        for file in gen_files:
            f = os.path.basename(file)
            fname, fext = os.path.splitext(file)
            sys.argv = [os.path.join(_filedir_, 'ingest_client_sc456.py'),
                        'store',
                        '<file>',
                        '--environment=current',
                        '--project=TEST',
                        '--SDC=SOC',
                        f'--username={Credentials["u"]}',
                        f'--password={Credentials["p"]}']
            if fext in ['.xml', '.XML']:
                logger.info(f' - {f} (XML)')
            else:
                logger.info(f' - {f}')
                sys.argv.append('--noXML')
            sys.argv[2] = file
            logger.debug(f'Calling: {sys.argv}')

            # Do the actual ingestion
            ab_ingest_fn()

    except Exception as ee:
        logger.error(str(ee))

    finally:
        sys.argv = saved_argv

def process_files(items):
    """
    Prepare input folder if we receive input file(s)
    """
    for item in items:
        realItem = os.path.realpath(item)
        if os.path.isdir(realItem):
            ingest_files(realItem)
        elif os.path.isfile(realItem):
            newDir = os.path.join(os.path.dirname(realItem), ".dir")
            newItem = os.path.join(newDir, os.path.basename(realItem))
            try:
                os.mkdir(newDir)
                _, ext = os.path.splitext(newItem)
                if ext == '.zip':
                    unZipToFolder(realItem, newDir)
                else:
                    os.link(realItem, newItem)
                ingest_files(newDir)
            except Exception as ee:
                logger.warning('Error: {}'.format(ee))
            finally:
                shutil.rmtree(newDir)


def main():
    """
    Process LE1 dissemination generated JSON file, creates the appropriate LE1 product
    data and metadata files, and ingest them in the EAS.
    """
    args = getArgs()
    configureLogs(logging.WARNING if args.quiet else \
                      (logging.DEBUG if args.debug else logging.INFO))

    logger.info('== EAS Ingestion Tool - Wrapper aroung EAS ingest script ==')

    try:
        process_files(items = args.files_or_folders)
    except Exception as ee:
        logger.error('Error: {}'.format(ee))
        #logger.debug(ee.with_traceback())


if __name__ == '__main__':
    main()
