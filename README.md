# EAS Ingestor Tool

The script `eas_ingestor.py` is the script to be used for ingestion of 
files and folder in the EAS.

It is a simple wrapper around the original script by A.Belikov.
The last original script used is `ingest_client_sc456.py`.

The script eas_ingestor.py will always be the one to use.  If new scripts
from A.Belikov or other source appear, the appropriate one will be called 
always from `eas_ingestor.py`.
